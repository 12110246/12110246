﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ThucHanh_3.Models
{
    public class Post
    {
        public int ID { set; get; }
        public string Title { set; get; }
        public string Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        public virtual ICollection<Comment> Comment { set; get; }
    }
}