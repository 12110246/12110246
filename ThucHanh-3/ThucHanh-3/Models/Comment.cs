﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ThucHanh_3.Models
{
    public class Comment
    {
        public int ID { set; get; }
        public string Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated {set; get;}
        public int LastTime
        {
            get
            {
                return(DateTime.Now - DateCreated).Minutes;
            }
        }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}