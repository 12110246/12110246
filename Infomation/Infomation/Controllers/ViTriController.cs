﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Infomation.Models;

namespace Infomation.Controllers
{
    public class ViTriController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /ViTri/

        public ActionResult Index()
        {
            return View(db.ViTris.ToList());
        }

        //
        // GET: /ViTri/Details/5

        public ActionResult Details(int id = 0)
        {
            ViTri vitri = db.ViTris.Find(id);
            if (vitri == null)
            {
                return HttpNotFound();
            }
            return View(vitri);
        }

        //
        // GET: /ViTri/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ViTri/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ViTri vitri)
        {
            if (ModelState.IsValid)
            {
                db.ViTris.Add(vitri);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vitri);
        }

        //
        // GET: /ViTri/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ViTri vitri = db.ViTris.Find(id);
            if (vitri == null)
            {
                return HttpNotFound();
            }
            return View(vitri);
        }

        //
        // POST: /ViTri/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ViTri vitri)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vitri).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vitri);
        }

        //
        // GET: /ViTri/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ViTri vitri = db.ViTris.Find(id);
            if (vitri == null)
            {
                return HttpNotFound();
            }
            return View(vitri);
        }

        //
        // POST: /ViTri/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ViTri vitri = db.ViTris.Find(id);
            db.ViTris.Remove(vitri);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}