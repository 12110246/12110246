﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Infomation.Models;

namespace Infomation.Controllers
{
    public class QuangCaoController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /QuangCao/

        public ActionResult Index()
        {
            return View(db.QuangCaos.ToList());
        }

        //
        // GET: /QuangCao/Details/5

        public ActionResult Details(int id = 0)
        {
            QuangCao quangcao = db.QuangCaos.Find(id);
            if (quangcao == null)
            {
                return HttpNotFound();
            }
            return View(quangcao);
        }

        //
        // GET: /QuangCao/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /QuangCao/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(QuangCao quangcao)
        {
            if (ModelState.IsValid)
            {
                db.QuangCaos.Add(quangcao);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(quangcao);
        }

        //
        // GET: /QuangCao/Edit/5

        public ActionResult Edit(int id = 0)
        {
            QuangCao quangcao = db.QuangCaos.Find(id);
            if (quangcao == null)
            {
                return HttpNotFound();
            }
            return View(quangcao);
        }

        //
        // POST: /QuangCao/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(QuangCao quangcao)
        {
            if (ModelState.IsValid)
            {
                db.Entry(quangcao).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(quangcao);
        }

        //
        // GET: /QuangCao/Delete/5

        public ActionResult Delete(int id = 0)
        {
            QuangCao quangcao = db.QuangCaos.Find(id);
            if (quangcao == null)
            {
                return HttpNotFound();
            }
            return View(quangcao);
        }

        //
        // POST: /QuangCao/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            QuangCao quangcao = db.QuangCaos.Find(id);
            db.QuangCaos.Remove(quangcao);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}