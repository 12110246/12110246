﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Infomation.Models;

namespace Infomation.Controllers
{
    public class LoaiTinController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /LoaiTin/

        public ActionResult Index()
        {
            return View(db.LoaiTins.ToList());
        }

        //
        // GET: /LoaiTin/Details/5

        public ActionResult Details(int id = 0)
        {
            LoaiTin loaitin = db.LoaiTins.Find(id);
            if (loaitin == null)
            {
                return HttpNotFound();
            }
            return View(loaitin);
        }

        //
        // GET: /LoaiTin/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /LoaiTin/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LoaiTin loaitin)
        {
            if (ModelState.IsValid)
            {
                db.LoaiTins.Add(loaitin);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(loaitin);
        }

        //
        // GET: /LoaiTin/Edit/5

        public ActionResult Edit(int id = 0)
        {
            LoaiTin loaitin = db.LoaiTins.Find(id);
            if (loaitin == null)
            {
                return HttpNotFound();
            }
            return View(loaitin);
        }

        //
        // POST: /LoaiTin/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LoaiTin loaitin)
        {
            if (ModelState.IsValid)
            {
                db.Entry(loaitin).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(loaitin);
        }

        //
        // GET: /LoaiTin/Delete/5

        public ActionResult Delete(int id = 0)
        {
            LoaiTin loaitin = db.LoaiTins.Find(id);
            if (loaitin == null)
            {
                return HttpNotFound();
            }
            return View(loaitin);
        }

        //
        // POST: /LoaiTin/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LoaiTin loaitin = db.LoaiTins.Find(id);
            db.LoaiTins.Remove(loaitin);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}