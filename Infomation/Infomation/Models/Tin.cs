﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infomation.Models
{
    public class Tin
    {
        public int TinId { get; set; }
        public string TieuDe { get; set; }
        public string TomTat { get; set; }
        public DateTime NgayDang { get; set; }
        public string NoiDung { get; set; }
        public int SoLanXem { get; set; }
        public int UserId { get; set; }
        public int LoaiTinId { get; set; }
        public string TinhTrang { get; set; }
        public virtual User User { get; set; }
    }
}