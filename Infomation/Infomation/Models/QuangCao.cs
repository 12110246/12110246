﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infomation.Models
{
    public class QuangCao
    {
        public int QuangCaoId { get; set; }
        public string MoTa { get; set; }
        public string Url { get; set; }
        public string UrlHinh { get; set; }
        public int LoaiTinId { get; set; }
        public int ViTriId { get; set; }
        public int KichThuoc { get; set; }
    }
}