﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infomation.Models
{
    public class Comment
    {
        public int CommentId { get; set; }
        public DateTime Ngay { get; set; }
        public string NoiDung { get; set; }
        public string Name { get; set; }
        public int TinId { get; set; }
    }
}