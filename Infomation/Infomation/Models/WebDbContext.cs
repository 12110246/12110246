﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Infomation.Models
{
    public class WebDbContext:DbContext
    {
        public DbSet<User> UserProfiles { get; set; }
        public DbSet<Tin> Tins { get; set; }
        public DbSet<LoaiTin> LoaiTins { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<QuangCao> QuangCaos { get; set; }
        public DbSet<ViTri> ViTris { get; set; }
    }
}