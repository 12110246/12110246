﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infomation.Models
{
    public class Rating
    {
        public int RatingId { get; set; }
        public string MoTa { get; set; }
        public int TinId { get; set; }
        public int SoLanChon { get; set; }
    }
}