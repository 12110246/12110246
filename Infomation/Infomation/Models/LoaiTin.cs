﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infomation.Models
{
    public class LoaiTin
    {
        public int LoaiTinId { get; set; }
        public string TenLT { get; set; }
        public string TinhTrang { get; set; }
    }
}