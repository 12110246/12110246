﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infomation.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Passwork { get; set; }
        public string GioiTinh { get; set; }
        public DateTime NgaySinh { get; set; }
        public string DiaChi { get; set; }
        public DateTime NgayDk { get; set; }
        public virtual ICollection<Tin> Tins { get; set; } 
    }
}