namespace Infomation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Passwork = c.String(),
                        GioiTinh = c.String(),
                        NgaySinh = c.DateTime(nullable: false),
                        DiaChi = c.String(),
                        NgayDk = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Tins",
                c => new
                    {
                        TinId = c.Int(nullable: false, identity: true),
                        TieuDe = c.String(),
                        TomTat = c.String(),
                        NgayDang = c.DateTime(nullable: false),
                        NoiDung = c.String(),
                        SoLanXem = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        LoaiTinId = c.Int(nullable: false),
                        TinhTrang = c.String(),
                    })
                .PrimaryKey(t => t.TinId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.LoaiTins",
                c => new
                    {
                        LoaiTinId = c.Int(nullable: false, identity: true),
                        TenLT = c.String(),
                        TinhTrang = c.String(),
                    })
                .PrimaryKey(t => t.LoaiTinId);
            
            CreateTable(
                "dbo.Ratings",
                c => new
                    {
                        RatingId = c.Int(nullable: false, identity: true),
                        MoTa = c.String(),
                        TinId = c.Int(nullable: false),
                        SoLanChon = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RatingId);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        CommentId = c.Int(nullable: false, identity: true),
                        Ngay = c.DateTime(nullable: false),
                        NoiDung = c.String(),
                        Name = c.String(),
                        TinId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CommentId);
            
            CreateTable(
                "dbo.QuangCaos",
                c => new
                    {
                        QuangCaoId = c.Int(nullable: false, identity: true),
                        MoTa = c.String(),
                        Url = c.String(),
                        UrlHinh = c.String(),
                        LoaiTinId = c.Int(nullable: false),
                        ViTriId = c.Int(nullable: false),
                        KichThuoc = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.QuangCaoId);
            
            CreateTable(
                "dbo.ViTris",
                c => new
                    {
                        ViTriId = c.Int(nullable: false, identity: true),
                        TenViTri = c.String(),
                    })
                .PrimaryKey(t => t.ViTriId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tins", new[] { "UserId" });
            DropForeignKey("dbo.Tins", "UserId", "dbo.Users");
            DropTable("dbo.ViTris");
            DropTable("dbo.QuangCaos");
            DropTable("dbo.Comments");
            DropTable("dbo.Ratings");
            DropTable("dbo.LoaiTins");
            DropTable("dbo.Tins");
            DropTable("dbo.Users");
        }
    }
}
