﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    //tao table :post
    public class Post
    {
        // cos bao nhieu truong thi cos bay nhieu doi tuong dc tao
        //khoa
        [Key]
        public int Id { set; get; }
        public String Title { set; get; }
        public String Body { set; get; }
    }
    //lop anh xa qua csdl
    public class BlogDBContext: DbContext
    {
        //DBset dong bo csdl
        public DbSet<Post> Posts { set; get; }
        public DbSet<Comment> Comments { set; get; }

    }
}