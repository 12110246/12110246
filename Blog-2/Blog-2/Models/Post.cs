﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    [Table("baiviet")]
    public class Post
    {
        [Range(2,100)]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int ID { set; get; }
        [Required]
        public string Title { set; get; }
        [StringLength(250,ErrorMessage="so luong ki tu trong khoang 10 - 250",MinimumLength=10)]
        public string Body { set; get; }
        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }
        public DateTime DateUpdated { set; get; }
        public virtual ICollection<Comment> Comment { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}