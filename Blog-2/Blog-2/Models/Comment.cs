﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Comment
    {
        public int ID { set; get; }
        public string Body { set; get; }
        public DateTime DateCreated { set; get; }
        public DateTime DateUpdated { set; get; }
        public string Author { set; get; }
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}