﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        public string Content { set; get;}
        public virtual ICollection<Post> Posts { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }

    }
}