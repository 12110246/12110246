namespace Blog_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.baiviet", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.baiviet", "Body", c => c.String(maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.baiviet", "Body", c => c.String());
            AlterColumn("dbo.baiviet", "Title", c => c.String());
        }
    }
}
